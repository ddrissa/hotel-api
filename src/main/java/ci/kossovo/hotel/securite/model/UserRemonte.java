package ci.kossovo.hotel.securite.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UserRemonte {

	private Long id;

	private String titulaire;

	private String name;

	private String username;

	private String email;

	private String password;
	private boolean accountNonExpired = true;

	private boolean accountNonLocked = true;

	private boolean credentialsNonExpired = true;

	private boolean enabled = true;
	private boolean provisoire = false;
	
	List<AppRole> roles;

	public UserRemonte() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserRemonte(Long id, String titulaire, String name, String username, String email, String password,
			boolean accountNonExpired, boolean accountNonLocked, boolean credentialsNonExpired, boolean enabled,
			boolean provisoire) {
		this.id = id;
		this.titulaire = titulaire;
		this.name = name;
		this.username = username;
		this.email = email;
		this.password = password;
		this.accountNonExpired = accountNonExpired;
		this.accountNonLocked = accountNonLocked;
		this.credentialsNonExpired = credentialsNonExpired;
		this.enabled = enabled;
		this.provisoire = provisoire;
	}

	
}
