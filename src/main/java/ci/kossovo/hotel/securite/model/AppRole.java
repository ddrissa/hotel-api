package ci.kossovo.hotel.securite.model;

import java.io.Serializable;


public class AppRole implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	
	private NomRole nom;

	private String description;

	public AppRole() {
		super();
	}

	public AppRole(NomRole nom, String desc) {
		this.nom = nom;
		this.description = desc;
	}

	public AppRole(NomRole nom) {
		this.nom = nom;
	}

	public NomRole getNom() {
		return nom;
	}

	public void setNom(NomRole nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
