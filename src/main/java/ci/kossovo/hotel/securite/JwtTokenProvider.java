package ci.kossovo.hotel.securite;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

@Component
public class JwtTokenProvider {
	private static Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

	@Value("${app.jwtSecret}")
	private String jwtSecret;

	@Value("${app.jwtExpirationInMs}")
	private int jwtExpirationInMs;

	
	public Long getUserIdFromJWT(String token) {

		Algorithm algorithm = Algorithm.HMAC512(jwtSecret);
		JWTVerifier verifier = JWT.require(algorithm).build();
		DecodedJWT jwt = verifier.verify(token);

		return Long.parseLong(jwt.getSubject());
	}

	
	public boolean validateToken(String authToken) {

		try {

			JWT.require(Algorithm.HMAC512(jwtSecret)).build().verify(authToken);
			return true;

		} catch (JWTVerificationException e) {
			logger.error(e.getMessage());
		}

		return false;

	}

}
