package ci.kossovo.hotel.securite;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ci.kossovo.hotel.outils.ConstantSecurite;
import ci.kossovo.hotel.securite.model.UserRemonte;

@Service
public class PersoUserDetailsService  {

	

	// Cette méthode est utilisée par JWTAuthenticationFilter
	public UserDetails loadUserById(Long id, String token) throws UsernameNotFoundException {
		String url = "http://localhost:8183/api/userremonte/" + id;
		// String url2= "http://localhost:8183/api/approles/"+id;
		
		System.out.println("************* stade 0 ***********");
		HttpHeaders requeHheaders = new HttpHeaders();
		requeHheaders.add(ConstantSecurite.TOKEN_HEADER, ConstantSecurite.TOKEN_PREFIX + token);
		//requeHheaders.add(" Accepter ", MediaType.APPLICATION_JSON_VALUE);

		HttpEntity<?> requestEntity = new HttpEntity<>(requeHheaders);

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<UserRemonte> reponseUser = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
				UserRemonte.class);
		System.out.println("************* stade 1 ***********");
		System.out.println(reponseUser.getStatusCodeValue());
		// ResponseEntity<AppRole> roles= restTemplate.exchange(url2, HttpMethod.GET,
		// requestEntity, AppRole.class);

		UserRemonte ur = reponseUser.getBody();
		return UserPrincipal.create(ur, ur.getRoles());
	}

	

}
