package ci.kossovo.hotel.securite;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ci.kossovo.hotel.securite.model.AppRole;
import ci.kossovo.hotel.securite.model.UserRemonte;





public class UserPrincipal implements UserDetails{
	private static final long serialVersionUID = 1L;

	private UserRemonte user;
	private Collection<? extends GrantedAuthority> authorities;
	
	
	public UserPrincipal(UserRemonte user, Collection<? extends GrantedAuthority> authorities) {
		this.user = user;
		this.authorities = authorities;
	}
	
	
	public static UserPrincipal create(UserRemonte user, List<AppRole> roles) {
		List<GrantedAuthority> authorities = roles.stream().map(r -> new SimpleGrantedAuthority(r.getNom().name()))
				.collect(Collectors.toList());
		return new UserPrincipal(user, authorities);
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return authorities;
	}

	@Override
	@JsonIgnore
	public String getPassword() {
		return user.getPassword();
	}

	@Override
	public String getUsername() {
		return user.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		return user.isAccountNonExpired();
	}

	@Override
	public boolean isAccountNonLocked() {
		return user.isAccountNonLocked();
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return user.isCredentialsNonExpired();
	}

	@Override
	public boolean isEnabled() {
		return user.isEnabled();
	}

	
	public Long getId() {
		return user.getId();
	}

	public String getName() {
		return user.getName();
	}

	public String getCni() {
		return user.getTitulaire();
	}

	public String getEmail() {
		return user.getEmail();
	}

}
