package ci.kossovo.hotel.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import ci.kossovo.hotel.securite.JwtAuthenticationEntryPoint;
import ci.kossovo.hotel.securite.JwtAuthenticationFilter;



@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true, jsr250Enabled = true)

public class SecurityConfig extends WebSecurityConfigurerAdapter {

	//private PersoUserDetailsService persoUserDetailsService;
	private JwtAuthenticationEntryPoint unauthorizedHandler;

	

	
	public SecurityConfig(JwtAuthenticationEntryPoint unauthorizedHandler) {
		this.unauthorizedHandler = unauthorizedHandler;
	}

	@Bean
	public JwtAuthenticationFilter jwtAuthenticationFilter() {
		return new JwtAuthenticationFilter();
	}

	/*
	 * @Bean public PasswordEncoder passwordEncoder() { return new
	 * BCryptPasswordEncoder(); }
	 */
	/*
	 * @Bean AuthenticationSuccessHandler myAuthenticationSuccessHandler() {
	 * System.out.println("***************** Avertissemement**********"); return new
	 * MySimpleUrlAuthenticationSuccessHandler(); }
	 */
	
	// car authenticationManager ceci ne fait que securite web et non methode
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return authenticationManager();
	}

	/*
	 * @Override protected void configure(AuthenticationManagerBuilder auth) throws
	 * Exception { auth
	 * .userDetailsService(persoUserDetailsService).passwordEncoder(passwordEncoder(
	 * )); }
	 */

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		System.out.println("************* stade 6 ***********");

		http
		.cors()
		.and()
		.csrf().disable()
		.exceptionHandling()
		.authenticationEntryPoint(unauthorizedHandler)
		.and()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and()
		.authorizeRequests()
		.antMatchers("/api/clients/**")
		.permitAll()
		.anyRequest()
		.authenticated();
		
		//Ajoutez notre filtre de sécurité JWT personnalisé
		http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
		System.out.println("************* stade 5 ***********");
	}

}
