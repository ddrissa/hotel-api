package ci.kossovo.hotel.documents;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Adresse {
	private String email;
	private String domicile;
	private String boitepostale;

}
