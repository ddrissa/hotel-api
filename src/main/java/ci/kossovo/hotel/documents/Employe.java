package ci.kossovo.hotel.documents;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Document
@Getter @Setter
@Builder
@ToString(callSuper=true)
@EqualsAndHashCode(callSuper=true)
public class Employe extends Personne {
	private Date embauche;
	private String photo;
	private String fonction;
	private String quartier;

}
