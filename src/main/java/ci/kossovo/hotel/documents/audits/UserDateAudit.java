package ci.kossovo.hotel.documents.audits;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ci.kossovo.hotel.documents.Personne;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@JsonIgnoreProperties(value = { "createdBy", "updatedBy" }, allowGetters = true)
public abstract class UserDateAudit extends DateAudit {
	
	@CreatedBy
	private Personne createdBy;

	@LastModifiedBy
	private Personne updatedBy;

}
