package ci.kossovo.hotel.documents;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import ci.kossovo.hotel.documents.audits.UserDateAudit;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Getter @Setter
@ToString(of= {"id","nom", "prenom","cni"})
@EqualsAndHashCode(of= {"id","cni"}, callSuper=false)
public abstract class Personne extends UserDateAudit {
	@Id
	private String id;
	private String nom;
	private String prenom;
	@Indexed(unique = true)
	private String cni;
	private String societe;
	private String profession;
	private Adresse adresse;
	private List<Telephone> telephones;
	private String nomComplet;
	
	
	public Personne() {
		super();
		this.telephones = new ArrayList<>();
		
	}
	
	

	public Personne(String nom, String prenom, String cni, String societe, String profession, Adresse adresse) {
		this.nom = nom;
		this.prenom = prenom;
		this.cni = cni;
		this.societe = societe;
		this.profession = profession;
		this.adresse = adresse;
	}



	public Personne(String nom, String prenom, String cni) {
		this.nom = nom;
		this.prenom = prenom;
		this.cni = cni;
	}

	
	
	
	
	
}
