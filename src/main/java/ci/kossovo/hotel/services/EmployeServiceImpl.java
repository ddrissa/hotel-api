package ci.kossovo.hotel.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import ci.kossovo.hotel.documents.Employe;
import ci.kossovo.hotel.exceptions.InvalidHotelException;
import ci.kossovo.hotel.repository.EmployeRepository;
import ci.kossovo.hotel.securite.model.UserRemonte;

@Service
public class EmployeServiceImpl implements IEmployeMetier {

	EmployeRepository employeRepository;

	public EmployeServiceImpl(EmployeRepository employeRepository) {
		this.employeRepository = employeRepository;
	}

	@Override
	public Employe save(Employe entity) throws InvalidHotelException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Employe> All(List<Employe> entity) throws InvalidHotelException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employe modifier(Employe entity) throws InvalidHotelException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<Employe> findById(String id) {
		return employeRepository.findById(id);
	}

	@Override
	public List<Employe> findAll() {
		return employeRepository.findAll();
	}

	@Override
	public void spprimer(List<Employe> entities) {

		employeRepository.deleteAll(entities);
	}

	@Override
	public boolean supprimer(String id) throws InvalidHotelException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean existe(String id) {
		return employeRepository.existsById(id);
	}

	@Override
	public Long compter() {
		return employeRepository.count();
	}

	@Override
	public List<Employe> trouverNomCompletParMc(String mc) {
		return employeRepository.findByNomCompletContainingIgnoreCase(mc);
	}

	@Override
	public Stream<Employe> findByStream() {
		return employeRepository.findBy();
	}

	@Override
	public Optional<UserRemonte> findMyUser(String cni) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean existsByCni(String cni) {
		return employeRepository.existsByCni(cni);
	}
	

}
