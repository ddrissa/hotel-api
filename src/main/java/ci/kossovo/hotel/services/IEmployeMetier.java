package ci.kossovo.hotel.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import ci.kossovo.hotel.documents.Employe;
import ci.kossovo.hotel.securite.model.UserRemonte;


public interface IEmployeMetier extends IMetier<Employe, String> {
	
	List<Employe> trouverNomCompletParMc(String mc);

	Stream<Employe> findByStream();

	Optional<UserRemonte> findMyUser(String cni);
	
	Boolean existsByCni(String cni);
	

}
