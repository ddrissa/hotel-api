package ci.kossovo.hotel.services;

import java.util.List;
import java.util.Optional;

import ci.kossovo.hotel.exceptions.InvalidHotelException;

public interface IMetier<T,U>{
	
	public T save(T entity) throws InvalidHotelException;
	public List<T> All(List<T> entity) throws InvalidHotelException;

	public T modifier(T entity) throws InvalidHotelException;

	public Optional<T> findById(U id);

	public List<T> findAll();

	public void spprimer(List<T> entities);
	//public void deleteaAll();

	public boolean supprimer(U id) throws InvalidHotelException;

	public boolean existe(U id);

	public Long compter();
	
	
}
