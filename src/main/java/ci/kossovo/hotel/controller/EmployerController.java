package ci.kossovo.hotel.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class EmployerController {
	
	@GetMapping("/employes")
	@PreAuthorize("hasRole('GERANT') AND hasRole('DBA')")
	public String getEmploye() {
		return " Si vous voyez ce message, alors restTemple a marche!";
	}

}
