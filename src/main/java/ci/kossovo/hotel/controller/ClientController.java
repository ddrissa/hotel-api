package ci.kossovo.hotel.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ClientController {
	
	@GetMapping("/clients")
	public String getEmploye() {
		return " Si vous voyez ce message, alors client sans authorisation marche!";
	}

}
