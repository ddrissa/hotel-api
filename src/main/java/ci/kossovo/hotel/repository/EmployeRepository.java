package ci.kossovo.hotel.repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.data.mongodb.repository.MongoRepository;

import ci.kossovo.hotel.documents.Employe;

public interface EmployeRepository extends MongoRepository<Employe, String> {
	List<Employe> findByNomCompletContainingIgnoreCase(String mc);
	Stream<Employe> findBy();
	//Employe findByLogin(String login);
	Optional<Employe> findByCni(String cni);
	Boolean existsByCni(String cni);

}
