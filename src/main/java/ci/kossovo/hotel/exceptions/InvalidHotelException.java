package ci.kossovo.hotel.exceptions;

public class InvalidHotelException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidHotelException(String message) {
		super(message);
	}
	
	

}
